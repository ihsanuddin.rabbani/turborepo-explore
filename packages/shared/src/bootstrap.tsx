import { createRoot } from 'react-dom/client';

const container = document.getElementById('app');
// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
const root = createRoot(container!);
root.render(
  <div>
    Nothing to see here. This package is intended to be
    used in other apps, not standalone
  </div>
);
