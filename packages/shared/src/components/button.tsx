import { ReactNode } from "react";

interface ButtonProps {
  children: ReactNode;
  className?: string;
  appName: string;
  type: 'submit' | 'button';
  color?: string;
  onClick?: () => void;
}

export const Button = ({ children, className, appName, type, color, onClick }: ButtonProps) => {
  return (
    <button
      className={className}
      onClick={() => {
        console.log(`Hello from your ${appName} app!`);
        onClick?.();
      }}
      style={{ padding: '12px', minWidth: '120px', backgroundColor: color ||'blue', color: 'white', outline: 'none', border: 'none', cursor: 'pointer' }}
      type={type}
    >
      {children}
    </button>
  );
};
