import React from 'react';
import { Button } from "shared/src/components";
import { addNumber } from 'shared/src/utils/calculate';

const App: React.FC = () => {
  const [count, setCount] = React.useState(0);

  return (
    <div style={{ borderRadius: '4px', border: '2px solid green', padding: '8px' }}>
      <h1>Orderbook packages</h1>
      <p>Counter: {count}</p>
      <Button appName='orderbook' type='button' color='blue' onClick={() => setCount(prev => addNumber(prev, 1))}>plus</Button>
      <Button appName='orderbook' type='button' color='red' onClick={() => setCount(prev => prev - 1)}>minus</Button>
    </div>
  )
}

export default App;
