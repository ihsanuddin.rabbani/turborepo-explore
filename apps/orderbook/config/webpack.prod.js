const HtmlWebpackPlugin = require("html-webpack-plugin");
const { ModuleFederationPlugin } = require('webpack').container;
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const { dependencies } = require('../package.json');
const path = require('path');
const { ProvidePlugin } = require('webpack');
const TerserPlugin = require('terser-webpack-plugin');

const sharedModule = {
  react: 'react',
  'react-dom' : 'react-dom'
};

const getSharedModulesConfig = (dependencies) => {
  return Object.values(sharedModule).reduce(
    (sharedModulesConfig, moduleName) =>
      dependencies[moduleName]
        ? {
            ...sharedModulesConfig,
            [moduleName]: {
              singleton: true,
              requiredVersion: dependencies[moduleName],
            },
          }
        : sharedModulesConfig,
    {}
  );
};

const prodConfig = {
  entry: './src/main',
  devtool: false,
  mode: 'production',
  optimization: {
    minimizer: [new TerserPlugin()],
  },
  output: {
    path: path.join(process.cwd(), 'dist'),
    chunkFilename: '[id].[contenthash].js',
    publicPath: 'auto',
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js'],
    plugins: [new TsconfigPathsPlugin()],
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.(png|jpg|jpeg)/,
        type: 'asset/resource',
      },
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'dts-loader',
            options: {
              name: 'orderbook',
              exposes: {
                './App': './src/App'
              },
              typesOutputDir: '.wp_federation',
            },
          },
        ],
      }
    ],
  },
  plugins: [
    new ProvidePlugin({
      React: 'react',
    }),
    new HtmlWebpackPlugin({
      template: "./public/index.html",
      publicPath: '/',
    }),
    new ModuleFederationPlugin({
      name: 'orderbookModule',
      filename: 'remoteEntry.js',
      exposes: {
        './App': './src/App'
      },
      shared: getSharedModulesConfig(dependencies)
    }),
  ]
};

module.exports = prodConfig;