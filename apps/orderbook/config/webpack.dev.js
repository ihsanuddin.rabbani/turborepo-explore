const { merge } = require('webpack-merge');
const HtmlWebpackPlugin = require("html-webpack-plugin");
const commonConfig = require("./webpack.common");
const { ModuleFederationPlugin } = require('webpack').container;
const path = require('path');
const { ProvidePlugin } = require('webpack');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const { dependencies } = require('../package.json');

const sharedModule = {
  react: 'react',
  'react-dom' : 'react-dom'
};

const getSharedModulesConfig = (dependencies) => {
  return Object.values(sharedModule).reduce(
    (sharedModulesConfig, moduleName) =>
      dependencies[moduleName]
        ? {
            ...sharedModulesConfig,
            [moduleName]: {
              singleton: true,
              requiredVersion: dependencies[moduleName],
            },
          }
        : sharedModulesConfig,
    {}
  );
};

const devConfig = {
  mode: 'development',
  entry: './src/main',
  output: {
    path: path.join(process.cwd(), 'dist'),
    chunkFilename: '[id].[contenthash].js',
    publicPath: 'auto',
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js'],
    plugins: [new TsconfigPathsPlugin()],
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.(png|jpg|jpeg)/,
        type: 'asset/resource',
      },
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'dts-loader',
            options: {
              name: 'orderbook',
              exposes: {
                './App': './src/App'
              },
              typesOutputDir: '.wp_federation',
            },
          },
        ],
      }
    ],
  },
  devServer: {
    static: {
      directory: path.join(process.cwd(), 'dist'),
    },
    port: 3003,
    devMiddleware: {
      writeToDisk: true,
    },
    historyApiFallback: true,
    hot: 'only',
  },
  plugins: [
    new ProvidePlugin({
      React: 'react',
    }),
    new ModuleFederationPlugin({
      name: 'orderbookModule',
      filename: 'remoteEntry.js',
      exposes: {
        './App': './src/App'
      },
      shared: getSharedModulesConfig(dependencies)
    }),
    new HtmlWebpackPlugin({
      template: "./public/index.html",
      publicPath: '/',
    })
  ]
}

module.exports = devConfig;

// module.exports = merge(commonConfig, devConfig);